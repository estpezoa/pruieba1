package cl.duoc.estebanpezoa_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class ListadoUsuariosActivity extends AppCompatActivity {

    private ListView lvListaUsuarios;
    private Button btnLSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);

        lvListaUsuarios = (ListView) findViewById(R.id.lvListaUsuarios);

        ListadoUsuariosAdapter adaptador =
                new ListadoUsuariosAdapter(this, BaseDeDatos.obtieneListadoUsuarios());

        lvListaUsuarios.setAdapter(adaptador);


        btnLSalir = (Button) findViewById(R.id.btnLSalir);

        btnLSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


}
