package cl.duoc.estebanpezoa_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnRCrearUusario, btnRVolver;
    private EditText etRNombre, etRContrasena, etRRepetirContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        btnRCrearUusario = (Button) findViewById(R.id.btnRCrearUsuario);
        btnRVolver = (Button) findViewById(R.id.btnRVolver);

        etRNombre = (EditText) findViewById(R.id.etRNombre);
        etRContrasena = (EditText) findViewById(R.id.etRContrasena);
        etRRepetirContrasena = (EditText) findViewById(R.id.etRRepetirContrasena);

        btnRCrearUusario.setOnClickListener(this);
        btnRVolver.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnRCrearUsuario) {
            if (etRNombre.getText().toString().trim().equals("") || etRContrasena.getText().toString().trim().equals("") || etRRepetirContrasena.getText().toString().trim().equals("")) {
                Toast.makeText(this, "Rellene todos los campos", Toast.LENGTH_SHORT).show();
            } else {
                if (etRContrasena.getText().toString().equals(etRRepetirContrasena.getText().toString())) {
                    Usuario u = new Usuario();

                    u.setNombre(etRNombre.getText().toString());
                    u.setClave(etRContrasena.getText().toString());


                    AgregarABaseDeDatos(u);


                    Toast.makeText(this, "Usuario agregado correctamente ", Toast.LENGTH_LONG).show();
                    etRNombre.setText("");
                    etRContrasena.setText("");
                    etRRepetirContrasena.setText("");
                } else {
                    Toast.makeText(this, "Contraseñas no coinciden", Toast.LENGTH_LONG).show();
                }
            }
        } else if (v.getId() == R.id.btnRVolver) {
            onBackPressed();
        }

        /*switch (v.getId())
        {
            case R.id.btnRCrearUsuario:
                break;
            case R.id.btnRVolver:
                break;
        }*/
    }

    private void AgregarABaseDeDatos(Usuario u) {
        BaseDeDatos.agregarUsuario(u);
    }

}
