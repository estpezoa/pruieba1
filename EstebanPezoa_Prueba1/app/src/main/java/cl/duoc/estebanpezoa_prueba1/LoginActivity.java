package cl.duoc.estebanpezoa_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAceptar, btnRegistrarse;
    private EditText etNombre, etContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnAceptar = (Button) findViewById(R.id.btnAceptar);
        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);


        btnAceptar.setOnClickListener(this);
        btnRegistrarse.setOnClickListener(this);

        etNombre = (EditText) findViewById(R.id.etNombre);
        etContrasena = (EditText) findViewById(R.id.etContrasena);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnAceptar) {
            if (etNombre.getText().toString().trim().equals("") || etContrasena.getText().toString().trim().equals("")) {
                Toast.makeText(this, "Rellene todos los campos", Toast.LENGTH_SHORT).show();
            } else {
                if (Existe() == true) {
                    Intent intent = new Intent(LoginActivity.this, ListadoUsuariosActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Usuario y contraseña no coinciden", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (v.getId() == R.id.btnRegistrarse) {
            Intent intent = new Intent(LoginActivity.this, RegistroActivity.class);
            startActivity(intent);
        }
    }

    private boolean Existe() {
        ArrayList<Usuario> listaTemp = BaseDeDatos.obtieneListadoUsuarios();

        for (Usuario x : listaTemp) {
            if (x.getNombre().equals(etNombre.getText().toString()) && x.getClave().equals(etContrasena.getText().toString())) {
                return true;
            }
        }
        return false;
    }
}
