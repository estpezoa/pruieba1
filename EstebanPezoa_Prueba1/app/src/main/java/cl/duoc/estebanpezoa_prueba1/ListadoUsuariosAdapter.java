package cl.duoc.estebanpezoa_prueba1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by DUOC on 21-04-2017.
 */

public class ListadoUsuariosAdapter extends ArrayAdapter<Usuario> {

    ArrayList<Usuario> datos = BaseDeDatos.obtieneListadoUsuarios();

    public ListadoUsuariosAdapter(Context context, ArrayList<Usuario> datos) {
        super(context, R.layout.listusuario_item, datos);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.listusuario_item, null);

        TextView tvNombre = (TextView)item.findViewById(R.id.tvnombre);
        tvNombre.setText(datos.get(position).getNombre().toString());

        return(item);
    }
}