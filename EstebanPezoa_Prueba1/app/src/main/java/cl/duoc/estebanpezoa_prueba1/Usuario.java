package cl.duoc.estebanpezoa_prueba1;

/**
 * Created by DUOC on 21-04-2017.
 */

public class Usuario {

    private String nombre;
    private String clave;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
